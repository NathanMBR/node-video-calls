import express from "express";
import cors from "cors";
import http from "http";
import { Server as WebSocketServer, Socket } from "socket.io";
import path from "path";
import routes from "./routes";

const userIdMap = new Map();

const app = express();
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(cors());

app.set("view engine", "ejs");
app.set("views", path.resolve(__dirname, "..", "views"));
app.use(express.static(path.resolve(__dirname, "..", "public")));

app.use(routes);

const server = http.createServer(app);
const io = new WebSocketServer(server);

io.on("connect", (socket: Socket) => {
	// Leave the own room that automatically joins in
	socket.leave(socket.id);

	// Join a room
	socket.on("join-room", (roomId: string, peerId: string) => {
		userIdMap.set(socket.id, peerId);
		
		socket.join(roomId);
		socket.to(roomId).emit("user-connected", peerId);
	});

	// Emitting disconnection notification to all in the room
	socket.on("disconnecting", () => {
		const { rooms } = socket;

		rooms.forEach(room => {
			socket.to(room).emit("user-disconnected", userIdMap.get(socket.id));

			userIdMap.delete(socket.id);
		});
	});
});

export default server;