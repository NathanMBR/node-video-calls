import { Router, Request, Response } from "express";
import { v4 as uuid } from "uuid";

const router = Router();

router.get(
	"/new",
	(_req: Request, res: Response) => res.redirect(`/room/${uuid().split("-", 1)[0]}`)
);

router.get(
	"/room/:id",
	(req: Request, res: Response) => res.render(
		"room",
		{
			roomId: req.params.id
		}
	)
);

export default router;