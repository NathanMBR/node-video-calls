import { PeerServer } from "peer";

const PEER_PORT = parseInt(`${process.env.PEER_PORT}`) || 4000;
const peer = PeerServer(
	{
		port: PEER_PORT,
		path: "/peer-server"
	}
);

peer.listen(() => console.log(`Server online in localhost:${PEER_PORT} at ${new Date}`));