import app from "./app";

const PORT = parseInt(`${process.env.PORT}`) || 3000;

app.listen(
	PORT,
	"0.0.0.0",
	() => {
		console.log(`Server online in localhost:${PORT} at ${new Date}`);
	}
);