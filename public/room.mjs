import { io } from "https://cdn.socket.io/4.3.1/socket.io.esm.min.js";
import { getRoomId } from "./modules/functions/getRoomId.mjs";
import { VideoCall } from "./modules/classes/VideoCall.mjs";

const roomId = getRoomId("#room-id");
const socket = io();

// Join room after connected
socket.on("connect", () => {
	console.log("My socket connection:", socket.id);

	/* eslint-disable-next-line no-undef */
	const peer = new Peer(undefined, { // PeerJS IDs must start and end with alphanumeric characters only
		debug: 2, // Default is 0 (see PeerJS documentation in API Reference -> Peer -> [options] -> debug)
		host: "/",
		port: 4000,
		path: "/peer-server"
	});
	
	const videoCall = new VideoCall({
		debug: true, // Default is false. If true, shows console messages for each method called
		socket,
		peer,
		roomId,
		videoHolderId: "video-holder",
		cameraResolution: {
			width: 1280,
			height: 720
		}
	});
	videoCall.init();
});