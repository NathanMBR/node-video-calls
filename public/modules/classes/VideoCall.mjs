import {
	DebugMessage,
	GetAvailableMediaDevices,
	GetLocalStream,
	GenerateVideoScreen,
	AppendVideoScreen,
	AddNewVideoScreen,
	RemoveDisconnectedUserVideo,
	SetupPeerConnection,
	RemoveJoinRoomScreen,
	AddVideoHolder,
	JoinRoom,
	SetupJoinRoom
} from "./static/index.mjs";

export class VideoCall {
	constructor(data) {
		this.debug = data.debug;
		this.socket = data.socket;
		this.peer = data.peer;
		this.videoHolderId = data.videoHolderId;
		this.cameraResolution = data.cameraResolution;
	}

	debugMessage = DebugMessage.execute;
	getAvailableMediaDevices = GetAvailableMediaDevices.execute;
	getLocalStream = GetLocalStream.execute;
	generateVideoScreen = GenerateVideoScreen.execute;
	appendVideoScreen = AppendVideoScreen.execute;
	addNewVideoScreen = AddNewVideoScreen.execute;
	removeDisconnectedUserVideo = RemoveDisconnectedUserVideo.execute;
	setupPeerConnection = SetupPeerConnection.execute;
	removeJoinRoomScreen = RemoveJoinRoomScreen.execute;
	addVideoHolder = AddVideoHolder.execute;
	joinRoom = JoinRoom.execute;
	setupJoinRoom = SetupJoinRoom.execute;

	async init() {
		this.debugMessage("Initiating...");

		this.setupPeerConnection();
		
		const availableDevices = await this.getAvailableMediaDevices();
		this.localStream = await this.getLocalStream(availableDevices);

		this.setupJoinRoom();

		this.debugMessage("Initialized.");
	}
}