export class JoinRoom {
	static execute() {
		this.debugMessage("Joining the room...");

		this.removeJoinRoomScreen();
		this.addVideoHolder();
		this.addNewVideoScreen(this.peer._id, this.localStream, true);
		this.socket.emit("join-room", this.roomId, this.peer._id);

		this.debugMessage("Room joined.");
	}
}