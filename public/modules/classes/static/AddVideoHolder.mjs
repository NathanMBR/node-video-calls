export class AddVideoHolder {
	static execute() {
		this.debugMessage("Adding video holder...");

		const videoHolder = document.createElement("div");
		videoHolder.id = this.videoHolderId;
		document.body.appendChild(videoHolder);

		this.debugMessage("Video holder added.");
	}
}