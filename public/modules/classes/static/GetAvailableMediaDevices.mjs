export class GetAvailableMediaDevices {
	static async execute() {
		try {
			this.debugMessage("Getting available media devices...");
	
			const availableDevices = {
				audio: false,
				video: false
			};
		
			const devices = await navigator.mediaDevices.enumerateDevices();
			devices.forEach(device => {
				const availableDevicesKeys = Object.keys(availableDevices);
		
				/* eslint-disable-next-line no-undef */
				if (device instanceof InputDeviceInfo) // Test if device is an input
					availableDevicesKeys.forEach(key => {
						const hasDevice = device.kind === `${key}input`;
								
						if (hasDevice)
							availableDevices[key] = true;
					});
			});
	
			if (availableDevices.video)
				availableDevices.video = this.cameraResolution;
			
			this.debugMessage("Available media devices gotten.");
	
			return availableDevices;
		} catch (error) {
			console.error("Failed to get the available devices. Error:", error);
		}
	}
}