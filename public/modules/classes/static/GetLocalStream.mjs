export class GetLocalStream {
	static async execute(availableMediaDevices) {
		try {
			this.debugMessage("Getting local stream...");

			if (!availableMediaDevices.audio && !availableMediaDevices.video)
				return undefined;

			const localStream = await navigator.mediaDevices?.getUserMedia(availableMediaDevices);

			this.debugMessage("Local stream gotten.");

			return localStream;
		} catch (error) {
			console.error("Failed to get the local stream. Error:", error);
		}
	}
}