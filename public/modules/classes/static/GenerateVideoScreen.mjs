export class GenerateVideoScreen {
	static execute(video, stream, socketId) {
		this.debugMessage("Generating video screen...");

		video.srcObject = stream;
		video.addEventListener("loadedmetadata", video.play);

		const videoGrid = document.createElement("div");
		videoGrid.classList.add("video-grid");
		videoGrid.setAttribute("data-id", socketId);
		videoGrid.appendChild(video);
		if (video.muted)
			videoGrid.classList.add("video-grid-local");

		this.debugMessage("Video screen generated.");
		
		return videoGrid;
	}
}