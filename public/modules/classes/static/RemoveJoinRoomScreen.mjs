export class RemoveJoinRoomScreen {
	static execute() {
		this.debugMessage("Removing join room screen...");

		const joinRoomScreen = document.querySelector("#join-room");

		joinRoomScreen ?
			document.body.removeChild(joinRoomScreen) :
			this.debugMessage("ERROR: join room screen not found.");

		this.debugMessage("Join room screen removed.");
	}
}