export class AddNewVideoScreen {
	static execute(id, stream, isMuted = false) {
		this.debugMessage("Adding new video screen...");

		const videoElement = document.createElement("video");
		videoElement.muted = isMuted;

		const videoScreen = this.generateVideoScreen(
			videoElement,
			stream,
			id
		);
		this.appendVideoScreen(videoScreen);

		this.debugMessage("New video screen added.");
	}
}