export class SetupPeerConnection {
	static execute() {
		this.debugMessage("Doing peer connection setup...");

		this.peer.on("error", console.error);
		this.peer.on("open", () => {
			console.log("My peer ID:", this.peer._id);
			this.peer.on("call", call => {
				console.log("New call from", call.peer);
				
				call.on("stream", stream => this.addNewVideoScreen(call, stream));
				call.answer(this.localStream);
			});

			this.socket.on("user-connected", id => {
				if (this.peer._id !== id) {
					console.log("New socket connection:", id);

					const call = this.peer.call(id, this.localStream);
					call.on("stream", stream => this.addNewVideoScreen(call, stream));
				}
			});
			this.socket.on("user-disconnected", id => {
				console.log("Disconnected:", id);
				this.removeDisconnectedUserVideo(id);
			});
		});

		this.debugMessage("Peer connection setup done.");
	}
}