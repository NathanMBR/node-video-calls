export class SetupJoinRoom {
	static execute() {
		this.debugMessage("Doing join room setup...");

		const btn = document.createElement("button");
		btn.onclick = () => this.joinRoom();
		btn.innerText = "Join room";
		document.querySelector("#join-room")?.appendChild(btn);

		this.debugMessage("Join room setup done.");
	}
}