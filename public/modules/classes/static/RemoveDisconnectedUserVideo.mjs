export class RemoveDisconnectedUserVideo {
	static execute(id) {
		this.debugMessage("Removing disconnected user video...");

		const videoHolder = document.getElementById(this.videoHolderId);
		const disconnectedUserVideo = document.querySelector(`[data-id='${id}']`);

		videoHolder && disconnectedUserVideo ?
			videoHolder.removeChild(disconnectedUserVideo) :
			this.debugMessage("ERROR: video holder element or disconnected user video not found.");

		this.debugMessage("Disconnected user video removed.");
	}
}