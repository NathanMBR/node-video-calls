export class AppendVideoScreen {
	static execute(videoScreen) {
		this.debugMessage("Appending video screen...");

		document.getElementById(this.videoHolderId)?.appendChild(videoScreen);

		this.debugMessage("Video screen appended.");
	}
}